# About

An s-expression based markup language that compiles to Pandoc Markdown.

# End User Tags

## Document 

(d ((Title of Document)
    (Author of Document)
    (Date of Document))
    headings ...)


The start of the document tag, list can be empty. 
All other tags must be enclosed within this tag.

## Headings 

(h (n Title of Heading ...) paragraphs ...)

The heading tag takes a n as the level of the heading. All paragraphs must be within one.

## Paragraph (p ...)

The paragraph tag treats all text as a single paragraph regardless of internal white spaces.

## Inline formatting (s ...)

Where s is one of b i m, etc. All text within the bracket is rendered in the form given.

## Tables 

TBD

## Lists

TBD

## Maths

TBD

## Code 

TDB

## Other

Heading identifiers
Block quotations
Indented code blocks
Horizontal rules
Metadata blocks
Backslash escapes
Generic raw attribute
Links
Automatic links
Inline links
Reference links
Internal links
Images
Divs and Spans
Footnotes
Citations


