(import (markdown)
        (scheme base)
        (scheme write))

(display  (d
           ((A Document) () ())
           (h (0 This is a Heading)
              (p This is some text. There is more of it here. And soon we will start a new
                 paragraph. Completely (b  ignoring any number) of whitespaces here!)
              (p This is other text. (quote (+ 1 2))))
           ))
