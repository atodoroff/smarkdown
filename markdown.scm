(define-library (markdown)
  (import (scheme base)
          (scheme write))
  (export d p h b)

  (begin

    (define-syntax d
      (syntax-rules ()
        ((_
          ((x ...) (y ...) (z ...))
          a ...)
         (stringify "%" x ... "\n%" y ... "\n%" z ... "\n\n"
                    a ...) )))

    (define-syntax p
      (syntax-rules ()
        ((_ a ...)
         (stringify a ... "\n\n"))))

    (define-syntax h
      (syntax-rules ()
        ((_ (n title ...) a ...)
         (stringify
          (string-append "\n" (n-hashes n))
          title ... "\n\n"
          a ...)
         )))

    (define-syntax b
      (syntax-rules ()
        ((_ a ...)
         (string-append "**" (stringify a ...) "**"))))

    (define n-hashes
      (lambda (n)
        (if (= n 0)
            "#"
            (begin
              (string-append "#"
                             (n-hashes (- n 1)))))))

    (define-syntax stringify
      (syntax-rules ()
        ((_ d)
         (let ((str (open-output-string))
               (qd (quote d)))
           (if (pair? qd)
               (display d str)
               (display qd str))
           (get-output-string str)))
        ((_ d0 d1 ...)
         (string-append
          (let ((str (open-output-string))
                (qd (quote d0)))
            (if (pair? qd)
                (display d0 str)
                (display qd str))
            (display " " str)
            (get-output-string str))
          (stringify d1 ...)))))))
